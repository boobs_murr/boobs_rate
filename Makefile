.EXPORT_ALL_VARIABLES:
GOBIN = $(shell pwd)/bin
GOFLAGS = -mod=vendor
GO111MODULE = on
SHELL=/bin/bash

.PHONY: deps
deps:
	@go mod download
	@go mod vendor
	@go mod tidy
	@./scripts/proto-third-party.sh

.PHONY: tool-grpc
tool-grpc:
	@cd tools ; ./install.sh

.PHONY: tools
tools: deps
	@go install github.com/gojuno/minimock/v3/cmd/minimock
	@go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate

.PHONY: build-binary
build:
	@go build -o ./bin/tits ./cmd/tits


.PHONY: test
test:
	@go test ./...


.PHONY: lint
lint:
	@golangci-lint run

.PHONY: clean
clean:
	@rm -fv ./bin/*

.PHONY: generate-grpc
generate-grpc: deps tool-grpc
	@./scripts/generate.sh

.PHONY: generate
generate: tools
	@export PATH=$(shell pwd)/bin:$(PATH); go generate ./...

.PHONY: migration
migration:
	@./bin/migrate create -ext sql -dir migrations -seq -digits 8 $(NAME)

#
# Protobuf
#

# PROTOC is required to generate code from proto files. If make target depends
# on it and it is not installed, make will be terminated.
PROTOC := $(shell which protoc)
ifeq ($(PROTOC),)
	PROTOC = must-install-protoc
endif

$(PROTOC):
	@echo -e "\nERROR: protobuf is not installed. Use your package manager to install or compile from source. See https://github.com/protocolbuffers/protobuf for details.\n"
	@exit 1
