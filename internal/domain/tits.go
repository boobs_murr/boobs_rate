package domain

import "time"

type Tits struct {
	ID           string    `json:"id"`
	CreatedAt    time.Time `json:"created_at"`
	Rating       int64     `json:"rating"`
	PeachRating  int64     `json:"peachRating"`
	CherryRating int64     `json:"cherryRating"`
	URL          string    `json:"url"`
}

type VoteType string

func (vt VoteType) String() string {
	return string(vt)
}

const (
	VoteTypePeach  VoteType = "PEACH"
	VoteTypeCherry VoteType = "CHERRY"
)
