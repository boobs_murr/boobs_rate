package postgres

import (
	"time"

	"github.com/uptrace/bun"
	"gitlab.com/boobs_murr/murr_boobs_rate_app/internal/domain"
)

type titsModel struct {
	bun.BaseModel `bun:"table:tits"`

	ID           string    `bun:"id,pk"`
	CreatedAt    time.Time `bun:"created_at"`
	Rating       int64     `bun:"rating"`
	PeachRating  int64     `bun:"peach_rating"`
	CherryRating int64     `bun:"cherry_rating"`
}

func (t *titsModel) FromDomain(tits domain.Tits) {
	t.CreatedAt = tits.CreatedAt
	t.Rating = tits.Rating
	t.PeachRating = tits.PeachRating
	t.CherryRating = tits.CherryRating
	t.ID = tits.ID
}

func titsModelToDomain(model titsModel) domain.Tits {
	return domain.Tits{
		ID:           model.ID,
		CreatedAt:    model.CreatedAt,
		Rating:       model.Rating,
		PeachRating:  model.PeachRating,
		CherryRating: model.CherryRating,
	}
}

func titsModelsToDomain(models []titsModel) []domain.Tits {
	tits := make([]domain.Tits, 0, len(models))
	for _, model := range models {
		tits = append(tits, titsModelToDomain(model))
	}
	return tits
}
