package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/uptrace/bun"
	"gitlab.com/boobs_murr/murr_boobs_rate_app/internal/domain"
)

type TitsRepository struct {
	db *bun.DB
}

func NewTitsRepo(db *bun.DB) *TitsRepository {
	return &TitsRepository{
		db: db,
	}
}

func (t *TitsRepository) GetTits(ctx context.Context, limit int) ([]domain.Tits, error) {
	titsModels := make([]titsModel, 0, limit)
	err := t.db.NewSelect().
		Model(&titsModels).
		Limit(limit).
		Order("?").
		Scan(ctx)
	if err != nil {
		return nil, fmt.Errorf("get tits from db: %w", err)
	}
	tits := titsModelsToDomain(titsModels)
	return tits, nil
}

func (t *TitsRepository) CreateTits(ctx context.Context, tits domain.Tits) error {
	model := titsModel{}
	model.FromDomain(tits)
	_, err := t.db.NewInsert().
		Model(&model).
		Exec(ctx)
	if err != nil {
		return fmt.Errorf("create tits in db: %w", err)
	}
	return nil
}

func (t *TitsRepository) IncreaseRating(ctx context.Context, titsID string) error {
	err := t.db.RunInTx(ctx, &sql.TxOptions{}, func(ctx context.Context, tx bun.Tx) error {
		_, err := tx.NewUpdate().
			Model(&titsModel{}).
			Set("rating = rating + 1").
			Where("id = ?", titsID).
			Exec(ctx)
		return err
	})
	if err != nil {
		return fmt.Errorf("increase rating in db: %w", err)
	}
	return nil
}

func (t *TitsRepository) VoteTits(ctx context.Context, titsID string, voteType domain.VoteType) error {
	err := t.db.RunInTx(ctx, &sql.TxOptions{}, func(ctx context.Context, tx bun.Tx) error {
		query := tx.NewUpdate().
			Model(&titsModel{}).
			Where("id = ?", titsID)

		switch voteType {
		case domain.VoteTypePeach:
			query.Set("peach_rating = peach_rating + 1")
		case domain.VoteTypeCherry:
			query.Set("cherry_rating = cherry_rating + 1")
		}

		_, err := query.Exec(ctx)
		return err
	})
	if err != nil {
		return fmt.Errorf("vote tits in db: %w", err)
	}
	return nil
}
