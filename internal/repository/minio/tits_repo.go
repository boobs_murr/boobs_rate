package minio

import (
	"context"
	"fmt"

	"github.com/minio/minio-go/v7"
)

const (
	titsBucketName  = "tits"
	titsContentType = "image/jpeg"
)

type TitsRepository struct {
	client *minio.Client
}

func NewTitsRepository(client *minio.Client) *TitsRepository {
	return &TitsRepository{
		client: client,
	}
}

func (t *TitsRepository) CreateTitsFromFile(ctx context.Context, imageName string, filePath string) error {
	_, err := t.client.FPutObject(
		ctx, titsBucketName, imageName, filePath, minio.PutObjectOptions{ContentType: titsContentType},
	)
	if err != nil {
		return fmt.Errorf("upload image to minio: %v", err)
	}
	return nil
}
