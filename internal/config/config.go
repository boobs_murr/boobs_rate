package config

import (
	"fmt"

	"github.com/caarlos0/env/v6"
)

type Configuration struct {
	Database DatabaseConfig
	Cache    CacheConfig
	Tracing  TracingConfig
	Metrics  MetricsConfig
	Storage  StorageConfig
}

type StorageConfig struct {
	Endpoint  string `env:"STORAGE_ENDPOINT" required:"true"`
	SecretID  string `env:"STORAGE_SECRET_ID" required:"true"`
	SecretKey string `env:"STORAGE_SECRET_KEY" required:"true"`
}

type CacheConfig struct {
	Host string `env:"CACHE_HOST" required:"true"`
	Port int    `env:"CACHE_PORT" required:"true"`
}

type MetricsConfig struct {
	Host string `env:"METRICS_HOST" required:"true"`
	Port int    `env:"METRICS_PORT" envDefault:"9090"`
}

type DatabaseConfig struct {
	Host     string `env:"DATABASE_HOST" required:"true"`
	Port     int    `env:"DATABASE_PORT" required:"true"`
	User     string `env:"DATABASE_USER" required:"true"`
	Password string `env:"DATABASE_PASSWORD" required:"true"`
	Name     string `env:"DATABASE_NAME" required:"true"`
}

func (d *DatabaseConfig) DSN() string {
	return fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable", d.User, d.Password, d.Host, d.Port, d.Name)
}

type TracingConfig struct {
	JaegerEndpoint string `env:"TRACING_JAEGER_ENDPOINT" required:"true"`
	TracerName     string `env:"TRACING_TRACER_NAME" required:"true"`
}

func LoadConfiguration() (*Configuration, error) {
	var config Configuration
	if err := env.Parse(&config); err != nil {
		return nil, err
	}
	return &config, nil
}
