package initiator

import (
	"context"
	"fmt"
	"io/ioutil"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"go.opentelemetry.io/otel"
	"go.uber.org/zap"
)

const titsPath = "images"

type Service struct {
	log         *otelzap.Logger
	titsService TitsService
}

func NewService(log *zap.Logger, titsService TitsService) *Service {
	return &Service{
		log:         otelzap.New(log.Named("initiator")),
		titsService: titsService,
	}
}

func (s *Service) Run() {
	s.log.Info("Starting")
	defer s.log.Info("Stopped")

	files, err := ioutil.ReadDir(titsPath)
	if err != nil {
		s.log.Error("Failed to read directory", zap.Error(err))
	}

	ctx := context.Background()
	ctx, span := otel.Tracer("initiator").Start(ctx, "run")
	defer span.End()

	for _, f := range files {
		s.log.Info("Creating new tits", zap.String("name", f.Name()))
		err := s.titsService.CreateTitsFromFile(ctx, f.Name(), fmt.Sprintf("%s/%s", titsPath, f.Name()))
		if err != nil {
			s.log.Error("Failed to create tits", zap.Error(err))
		}
	}
}
