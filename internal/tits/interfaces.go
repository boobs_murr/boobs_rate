package tits

import (
	"context"

	"gitlab.com/boobs_murr/murr_boobs_rate_app/internal/domain"
)

type Database interface {
	GetTits(ctx context.Context, limit int) ([]domain.Tits, error)
	CreateTits(ctx context.Context, tits domain.Tits) error
	IncreaseRating(ctx context.Context, titsID string) error
	VoteTits(ctx context.Context, titsID string, voteType domain.VoteType) error
}

type Storage interface {
	CreateTitsFromFile(ctx context.Context, imageName string, filePath string) error
}
