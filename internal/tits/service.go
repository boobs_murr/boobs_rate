package tits

import (
	"context"
	"strings"
	"time"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/boobs_murr/murr_boobs_rate_app/internal/domain"
	"go.opentelemetry.io/otel"
	"go.uber.org/zap"
)

const defaultTitsCreateTimeout = time.Second * 10

type Service struct {
	db      Database
	storage Storage

	log *otelzap.Logger
}

func NewService(db Database, storage Storage, log *zap.Logger) *Service {
	return &Service{
		db:      db,
		storage: storage,
		log:     otelzap.New(log.Named("tits_service")),
	}
}

func (s *Service) CreateTitsFromFile(ctx context.Context, filename, filePath string) error {
	ctx, cancel := context.WithTimeout(ctx, defaultTitsCreateTimeout)
	defer cancel()
	ctx, span := otel.Tracer("tits_service").Start(ctx, "tits.Service.CreateTitsFromFile")
	defer span.End()
	err := s.storage.CreateTitsFromFile(ctx, filename, filePath)
	if err != nil {
		span.RecordError(err)
		s.log.Ctx(ctx).Error("failed to create tits from file", zap.Error(err))
		return err
	}
	err = s.db.CreateTits(ctx, domain.Tits{
		ID:           strings.ReplaceAll(filename, ".jpg", ""),
		CreatedAt:    time.Now().UTC(),
		Rating:       0,
		PeachRating:  0,
		CherryRating: 0,
	})
	if err != nil {
		span.RecordError(err)
		s.log.Ctx(ctx).Error("failed to create tits in db", zap.Error(err))
		return err
	}
	return nil
}
