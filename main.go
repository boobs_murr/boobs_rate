package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	minio2 "gitlab.com/boobs_murr/murr_boobs_rate_app/internal/repository/minio"
)

func main() {
	minioClient, err := minio.New("storage.ops.boobsrate.com", &minio.Options{
		Creds:  credentials.NewStaticV4("golangbackend", "142701foobar", ""),
		Secure: true,
	})

	if err != nil {
		panic(err)
	}

	repo := minio2.NewTitsRepository(minioClient)

	files, err := ioutil.ReadDir("images")
	if err != nil {
		log.Fatal(err)
	}
	for _, f := range files {
		fmt.Println(f.Name())
		err := repo.CreateTitsFromFile(context.Background(), f.Name(), "images/"+f.Name())
		if err != nil {
			log.Fatal(err)
		}
	}
}
