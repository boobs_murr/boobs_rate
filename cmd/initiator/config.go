package main

import (
	"fmt"

	"github.com/caarlos0/env/v6"
)

// Configuration represents application configuration for serve action.
type Configuration struct {
	Database DatabaseConfig
	Minio    MinioConfig
	Tracing  TracingConfig
}

type TracingConfig struct {
	JaegerEndpoint string `env:"TRACING_JAEGER_ENDPOINT" required:"true" envDefault:"https://tempo.jaeger.ops.boobsrate.com/api/traces"`
	TracerName     string `env:"TRACING_TRACER_NAME" required:"true" envDefault:"initiator"`
}

type MinioConfig struct {
	Endpoint  string `env:"MINIO_ENDPOINT" envDefault:"storage.ops.boobsrate.com"`
	AccessKey string `env:"MINIO_ACCESS_KEY" envDefault:""`
	SecretKey string `env:"MINIO_SECRET_KEY" envDefault:""`
	UseSSL    bool   `env:"MINIO_USE_SSL" envDefault:"true"`
}

type DatabaseConfig struct {
	Host          string `env:"DATABASE_HOST" required:"true" envDefault:"localhost"`
	Port          int    `env:"DATABASE_PORT" required:"true" envDefault:"5432"`
	User          string `env:"DATABASE_USER" required:"true" envDefault:"tits"`
	Password      string `env:"DATABASE_PASSWORD" required:"true" envDefault:"tits"`
	Name          string `env:"DATABASE_NAME" required:"true" envDefault:"tits"`
	MigrationsDir string `env:"MIGRATIONS_DIR" envDefault:"migrations/"`
}

func (d *DatabaseConfig) DSN() string {
	return fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable", d.User, d.Password, d.Host, d.Port, d.Name)
}

// LoadConfiguration returns a new application configuration parsed from environment variables.
func LoadConfiguration() (*Configuration, error) {
	var config Configuration
	if err := env.Parse(&config); err != nil {
		return nil, err
	}

	return &config, nil
}
