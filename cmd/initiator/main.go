package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"gitlab.com/boobs_murr/murr_boobs_rate_app/internal/applications/initiator"
	minio2 "gitlab.com/boobs_murr/murr_boobs_rate_app/internal/repository/minio"
	"gitlab.com/boobs_murr/murr_boobs_rate_app/internal/repository/postgres"
	"gitlab.com/boobs_murr/murr_boobs_rate_app/internal/tits"
	"gitlab.com/boobs_murr/murr_boobs_rate_app/pkg/tracing"
	"go.uber.org/zap"
)

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		panic(fmt.Errorf("creating logger: %w", err))
	}
	logger = logger.Named("migrate")
	defer logger.Sync() // nolint: errcheck

	cfg, err := LoadConfiguration()
	if err != nil {
		log.Fatal("load configuration: ", zap.Error(err))
	}

	tr, err := tracing.NewTracingProvider(cfg.Tracing.JaegerEndpoint, cfg.Tracing.TracerName)
	defer func() {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
		defer cancel()
		if err := tr.Shutdown(ctx); err != nil {
			log.Fatal(err)
		}
	}()

	if err != nil {
		log.Fatal("create tracer: ", zap.Error(err))
	}

	pgDB := postgres.NewPostgresDatabase(cfg.Database.DSN())
	titsRepo := postgres.NewTitsRepo(pgDB)

	minioClient, err := minio.New(cfg.Minio.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(cfg.Minio.AccessKey, cfg.Minio.SecretKey, ""),
		Secure: true,
	})
	if err != nil {
		logger.Fatal("creating minio client: ", zap.Error(err))
	}
	titsStorage := minio2.NewTitsRepository(minioClient)
	titsService := tits.NewService(titsRepo, titsStorage, logger)

	initiatorApp := initiator.NewService(logger, titsService)
	initiatorApp.Run()
}
