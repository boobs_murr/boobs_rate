#!/usr/bin/env bash
set -euo pipefail


protoc -I . \
  -I third_party/ \
  --plugin=./bin/protoc-gen-go \
  --go_out=. \
  --go_opt=paths=source_relative \
  --plugin=./bin/protoc-gen-go-grpc \
  --go-grpc_out=. \
  --go-grpc_opt=paths=source_relative \
  apis/tits/v1/*.proto


protoc -I . \
  -I third_party/ \
  --plugin=./bin/protoc-gen-grpc-gateway \
  --grpc-gateway_out=logtostderr=true,paths=source_relative:. \
  apis/tits/v1/*.proto

protoc -I . \
  -I third_party/ \
  --plugin=./bin/protoc-gen-swagger \
  --swagger_out=. \
  --swagger_opt=allow_merge=true \
  --swagger_opt=merge_file_name=apis/tits/v1/tits \
  --swagger_opt=fqn_for_swagger_name=true \
  --swagger_opt=simple_operation_ids=true \
  --swagger_opt=logtostderr=true \
  apis/tits/v1/*.proto