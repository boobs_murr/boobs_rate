package grpc

import (
	"time"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

const (
	defaultServerKeepaliveTime     = time.Minute * 5
	defaultServerKeepaliveTimeout  = time.Second * 20
	defaultServerKeepaliveAge      = time.Minute * 5
	defaultServerKeepaliveAgeGrace = time.Minute * 1
)

// Server can register itself to the gRPC server.
type Server interface {
	Register(srv *grpc.Server)
}

func NewGrpcServer(services []Server) *grpc.Server {
	unaryInterceptors := []grpc.UnaryServerInterceptor{
		grpc_prometheus.UnaryServerInterceptor,
		otelgrpc.UnaryServerInterceptor(),
	}

	srv := grpc.NewServer(
		grpc_middleware.WithUnaryServerChain(unaryInterceptors...),
		grpc.KeepaliveParams(keepalive.ServerParameters{
			MaxConnectionAge:      defaultServerKeepaliveAge,
			MaxConnectionAgeGrace: defaultServerKeepaliveAgeGrace,
			Time:                  defaultServerKeepaliveTime,
			Timeout:               defaultServerKeepaliveTimeout,
		}),
	)

	for _, reg := range services {
		reg.Register(srv)
	}

	grpc_prometheus.Register(srv)
	grpc_prometheus.EnableHandlingTimeHistogram()

	return srv
}
