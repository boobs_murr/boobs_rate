package server

import (
	"context"
	"net/http"
	"sync"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"
)

type GracefulServer struct {
	server *http.Server
	log    *otelzap.Logger

	wg   sync.WaitGroup
	dead chan struct{}
}

func NewGracefulServer(server *http.Server, log *otelzap.Logger) *GracefulServer {
	return &GracefulServer{
		log:    log,
		server: server,
	}
}

func (s *GracefulServer) Serve() {
	s.log.Info("Server starting...")
	defer s.log.Info("Server started")
	s.wg.Add(1)
	go func() {
		if err := s.server.ListenAndServe(); err != http.ErrServerClosed {
			close(s.dead)
		}
		s.wg.Done()
	}()
}

func (s *GracefulServer) Shutdown(ctx context.Context) error {
	s.log.Info("Server stopping...")
	defer s.log.Info("Server stopped")
	return s.server.Shutdown(ctx)
}

func (s *GracefulServer) Dead() <-chan struct{} {
	return s.dead
}
